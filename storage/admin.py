from django.contrib import admin
from storage import models

# Register your models here.

admin.site.register(models.Storage)
admin.site.register(models.Fact)
admin.site.register(models.Dimension)
admin.site.register(models.History)