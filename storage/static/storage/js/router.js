/* Artem Ustimov (c) 2016 */

define(function (require) {

    var Backbone = require('backbone'),
        session = require('models/session')/*,
        user = require('models/user')*/,
        viewManager = require('views/manager');

    //noinspection UnnecessaryLocalVariableJS
    var Router = Backbone.Router.extend({

        routes: {
            'list': 'listAction',
            'game': 'gameAction',
            'login': 'loginAction',
            'registration': 'regAction',
            'logout': 'logoutAction',
            '*default': 'defaultActions'
        },

        initialize: function () {
            this.listenTo(session, 'loginOk', this.defaultActions);
            /*
            this.listenTo(user, 'registerOk', this.defaultActions);
             */
        },

        defaultActions: function () {
            console.log("LIST");
            this.navigate('/#');
            viewManager.show('list');

        },

        scoreboardAction: function () {
            //viewManager.show('scoreboard');
        },

        gameAction: function () {
            //viewManager.show('game');
        },

        loginAction: function () {
            viewManager.show('login');
        },

        regAction: function () {
            //viewManager.show('reg');
        },

        logoutAction: function () {
            //viewManager.show('logout');
        }

    });

    return Router;

});