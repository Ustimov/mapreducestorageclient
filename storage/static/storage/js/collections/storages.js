/* Artem Ustimov (c) 2016 */

define(function (require) {

    var Backbone = require('backbone'),
        StorageModel = require('models/storage');

    //noinspection UnnecessaryLocalVariableJS
    var StorageCollection = Backbone.Collection.extend({

        url: '/api/list/',
        model: StorageModel

    });

    return new StorageCollection();

});