/* Artem Ustimov (c) 2016 */

define(function (require) {

    var Backbone = require('backbone');

    //noinspection UnnecessaryLocalVariableJS
    var StorageModel = Backbone.Model.extend({

        url: '/api/storage/:id',
        defaults: {
            id: -1,
            name: ''
        }

    });

    return StorageModel;

});