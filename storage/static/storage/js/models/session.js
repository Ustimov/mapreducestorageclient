/* Artem Ustimov (c) 2016 */

define(function (require) {

    var Backbone = require('backbone');

    //noinspection UnnecessaryLocalVariableJS
    var SessionModel = Backbone.Model.extend({

        url: '/api/login/',
        defaults: {
            isAuth: false
        }

    });

    return new SessionModel();

});