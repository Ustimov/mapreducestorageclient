/* Artem Ustimov (c) 2016 */

define(function (require) {

    var Backbone = require('backbone'),
        BaseView = require('views/base'),
        tmpl = require('tmpl/login'),
        session = require('models/session');

    //noinspection UnnecessaryLocalVariableJS
    var LoginView = BaseView.extend({

        template: tmpl,
        events: {
            'submit .js-submit': 'login'
        },

        login: function (event) {
            event.preventDefault();

            session.save({
                'username': event.target.username.value,
                'password': event.target.password.value
            }, {
                success: function (model, response) {
                    model.trigger('loginOk');
                },
                error: function (model, error) {
                    alert(error);
                }
            });
            /*
            this.$alert.html('');`

            var regExp = /^[a-z0-9]+$/i;

            if (!regExp.test(event.target.login.value) || !regExp.test(event.target.password.value)) {
                this.$alert.html('Логин и пароль должны содержать только цифры и латинские буквы');
                return;
            }

            if (event.target.password.value.length < 6) {
                this.$alert.html('Пароль не должен быть короче 6 символов');
                return;
            }
            */
            //session.login(event.target.login.value, event.target.password.value);
        }

    });

    return LoginView;

});