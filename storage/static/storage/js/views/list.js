/* Artem Ustimov (c) 2016 */

define(function (require) {

    var BaseView = require('views/base'),
        tmpl = require('tmpl/list'),
        storages = require('collections/storages');

    //noinspection UnnecessaryLocalVariableJS
    var ListView = BaseView.extend({
        template: tmpl,

        initialize: function () {
            this.listenTo(storages, 'add', this.render);
            this.listenTo(storages, 'loginRequired', this.loginRequired)
        },

        render: function () {
            this.$el.html(this.template(storages.toJSON()));
        },

        show: function () {
            storages.fetch({
                success: function (collection, response) {
                    if (response.status === 1) {
                        collection.trigger('loginRequired');
                    }
                },
                error: function (collection, response) {
                    console.log(response);
                }
            });
            this.trigger('show');
            this.$el.show();
        }

    });

    return ListView;

});