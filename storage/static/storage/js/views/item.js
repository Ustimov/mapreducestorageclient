/* Artem Ustimov (c) 2016 */

define(function (require) {

    var BaseView = require('views/base'),
        tmpl = require('tmpl/item');

    //noinspection UnnecessaryLocalVariableJS
    var StorageItemView = BaseView.extend({
        template: tmpl,
        tagName: 'li',
        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });

    return StorageItemView;

});