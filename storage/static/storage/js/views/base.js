define(function (require) {

    var Backbone = require('backbone');

    //noinspection UnnecessaryLocalVariableJS
    var BaseView = Backbone.View.extend({
        initialize: function () {
            this.render();
        },
        render: function () {
            this.$el.html(this.template);
        },
        show: function () {
            this.trigger('show');
            this.$el.show();
        },
        hide: function () {
            this.$el.hide();
        },
        loginRequired: function () {
            this.trigger('loginRequired');
        }
    });

    return BaseView;

});