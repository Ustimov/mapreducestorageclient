/* Artem Ustimov (c) 2016 */

require.config({
    urlArgs: "_=" + (new Date()).getTime(),
    baseUrl: "/static/storage/js",
    paths: {
        jquery: "lib/jquery",
        underscore: "lib/underscore",
        backbone: "lib/backbone"
    },
    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});

define(function (require) {
    var Backbone = require('backbone'),
        session = require('models/session');

    session.fetch({
        success: function (model, response) {
            console.log(response);
            Router = require('router');
            var router = new Router();
            Backbone.history.start();
        }
    });


});
