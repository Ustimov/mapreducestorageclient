from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login as login_user, logout as logout_user
from django.core.files import File
from json import loads, dumps
from subprocess import Popen, PIPE, STDOUT
from sys import platform
from os import listdir, remove
from datetime import datetime
from storage import models
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from storage.forms import UploadForm
import mysql.connector
import timeit

# Create your views here.

def run_command(command):
    p = Popen(command, stdout=PIPE, stderr=STDOUT)
    return iter(p.stdout.readline, b'')

#def list(request):
#    if not request.user.is_authenticated():
#        return JsonResponse({"status": 1, "message": "You should be authenticated"})
#    storages = models.Storage.objects.all()
#    return JsonResponse([{"id": storage.id, "name": storage.name} for storage in storages], safe=False)

def detail(request, id):
    try:
        storage = models.Storage.objects.get(id=id)
    except models.Storage.DoesNotExist:
        return HttpResponse("Storage does not exist")

    fks = {}
    facts = models.Fact.objects.filter(storage=id)
    for fact in facts:
        fks[fact] = models.Dimension.objects.filter(fact=fact)

    return render(request, 'storage/detail.html', {"storage": storage, "fks": fks})

def list(request):
    print("LOGIN POST {0}".format(str(request.body)))
    return render(request, 'storage/list.html', {'storages': models.Storage.objects.all()})

#def login(request):
#    if request.method == 'GET':
#        return JsonResponse({"isAuth": request.user.is_authenticated()})
#
#    body = loads(request.body.decode())
#    user = authenticate(username=body['username'], password=body['password'])
#    if not user:
#        return JsonResponse({"status": 2, "message": "User with such credinals not found"})
#    login_user(request, user)
#
#    return JsonResponse({"isAuth": True})


def logout(request):
    logout_user(request)
    return HttpResponseRedirect('/')


def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            login_user(request, user)
            success_url = request.GET.get('next')
            return HttpResponseRedirect(success_url)
    else:
        form = AuthenticationForm()
    return render(request, 'storage/login.html', {'form': form})


def create(request):
    return render(request, 'storage/create.html')
    #name = request.GET.get("name")
    #if name:
    #    models.Storage.objects.create(name=name)
    #return HttpResponse(name)


def index(requets):
    return HttpResponseRedirect('/list/')


def initialize(request, id):
    try:
        storage = models.Storage.objects.get(id=id)
        # Удаляем хранилище, если оно существовало
        for line in run_command(["{0}/bash/rm.sh".format(settings.BASE_DIR), storage.name]):
            print("RM: ", line)
        # Создаём папку для хранилища
        for line in run_command(["{0}/bash/mkdir.sh".format(settings.BASE_DIR), storage.name]):
            print("MKDIR: ", line)
    except models.Storage.DoesNotExist:
        return HttpResponse("Storage does not exist")

    fks = {}
    # Получаем все атрибуты таблицы хранилища
    facts = models.Fact.objects.filter(storage=id)
    for fact in facts:
        # Если атрибут является внешним ключём, то получаем все его измерения
        if fact.is_fk:
            dims = models.Dimension.objects.filter(fact=fact)
            fks[fact.name] = [dim.value for dim in dims]
        else:
            for line in run_command(["{0}/bash/touch.sh".format(settings.BASE_DIR), "/{0}/input/".format(storage.name), fact.name]):
                print("TOUCH: ", line)
    for key in fks.keys():
        for line in run_command(["{0}/bash/touch.sh".format(settings.BASE_DIR), "/{0}/dim/".format(storage.name), key]):
            print("TOUCH: ", line)
        #for attr in fks[key]:
        #    for line in run_command(["{0}/bash/append.sh".format(settings.BASE_DIR), attr, "/{0}/dim/{1}".format(storage.name, key)]):
        #       print("APPEND: ", line)
    for line in run_command(["{0}/bash/touch.sh".format(settings.BASE_DIR), "/{0}/input/".format(storage.name), "fks"]):
        print("TOUCH: ", line)

    return HttpResponseRedirect('/detail/%s/' % id)


def run(request, id):
    storage = models.Storage.objects.get(id=id)
    for line in run_command(["hadoop", "jar", "{0}/jar/MFRJ.jar".format(settings.BASE_DIR), "MFRJ", "/" + storage.name, "4", "[]"]):
        print("RUN: ", line)
    return HttpResponse("Run {0}".format(id))


def save(request):
    print(request.POST)
    if 'storage' in request.POST:
        storage = models.Storage.objects.create(name=request.POST.get('storage'), user=request.user)
    if 'attr' in request.POST:
        attrs = request.POST.getlist('attr')
        for fact_name in attrs:
            fact = models.Fact.objects.create(storage=storage, name=fact_name)
            checkbox = 'fact' + str(attrs.index(fact_name) + 1)
            if checkbox in request.POST:
                fact.is_fk = True
                fact.save()

            #if dims in request.POST:
            #    fact.is_fk = True
            #    fact.save()
            #    for dim_value in request.POST.getlist(dims):
            #        models.Dimension.objects.create(fact=fact, value=dim_value)

    return HttpResponseRedirect('/list/')


def query(request, id):
    print(request.POST)

    attrs = request.POST.getlist('fact')
    ops = request.POST.getlist('cond')
    values = request.POST.getlist('value')

    print('FACTS: ', attrs)
    print('COND: ', ops)
    print('VALUE: ', values)

    cond = []
    for i in range(len(attrs)):
        cond.append({'fact': attrs[i], 'operation': ops[i], 'value': values[i], 'type': 'numeric'})
    select = dumps(cond).replace('"',"'")

    print(select)

    storage = models.Storage.objects.get(id=id)
    attrs = models.Fact.objects.filter(storage=id)
    for line in run_command(["hadoop", "jar", "{0}/jar/MFRJ.jar".format(settings.BASE_DIR), "MFRJ", "/" + storage.name, str(len(attrs)), select]):
        print("RUN: ", line)

    for line in run_command(["hadoop", "fs", "-get", "/{0}/output/part*".format(storage.name)]):
        print("GET: ", line)
    filename = "{0} {1}".format(storage.name, datetime.now())
    res = open("{0}/{1}".format(settings.BASE_DIR, filename), "a+")
    for file in listdir(settings.BASE_DIR):
        if file.startswith("part"):
            print(file)
            part = open(file, 'r')
            for line in part.readlines():
                num, json = line.split('\t')
                res.write(json)
            part.close()
            remove(file)
    history = models.History.objects.create(storage=storage, user=request.user)
    history.result.save(filename, File(res))
    history.save()
    res.close()
    remove("{0}/{1}".format(settings.BASE_DIR, filename))
    for line in run_command(["{0}/bash/rm.sh".format(settings.BASE_DIR), "{0}/output/".format(storage.name)]):
        print("RM: ", line)
    return HttpResponseRedirect('/history/{0}'.format(id))


def result(request):
    context = {
        "headers": ['Город', 'Улица', 'Цена', 'Количество', 'Менеджер'],
        "data": [
            ['Арзамас', 'Архитектурная', '100', '1000', 'Вася'],
            ['Москва', 'Черепановых', '111', '1111', 'Коля'],
            ['СПБ', 'Никольская', '222', '2222', 'Захар'],
        ]
    }
    return render(request, 'storage/result.html', context)


def upload_data(request, id):
    storage = models.Storage.objects.get(id=id)
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            fks = []
            no_fks = []
            facts = models.Fact.objects.filter(storage=id)
            for fact in facts:
                if fact.is_fk:
                    fks.append(fact.name)
                else:
                    no_fks.append(fact.name)
            file = request.FILES['file']
            #print(file, type(file), dir(file))
            for line in file.readlines():
                line = line.decode()
                record = loads(line)
                fk_record = {}
                for fk in fks:
                    fk_record[fk] = record[fk]
                for l in run_command(["{0}/bash/append.sh".format(settings.BASE_DIR), dumps(fk_record), "/{0}/input/{1}".format(storage.name, "fks")]):
                    print("APPEND: ", l)
                for no_fk in no_fks:
                    for l in run_command(["{0}/bash/append.sh".format(settings.BASE_DIR), record[no_fk], "/{0}/input/{1}".format(storage.name, no_fk)]):
                        print("APPEND: ", l)
            return HttpResponseRedirect('/detail/{0}/'.format(id))
    else:
        form = UploadForm()
    return render(request, 'storage/upload.html', {'form': form, 'storage': storage})


def upload_values(request, id):
    fact = models.Fact.objects.get(id=id)
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            for line in run_command(["{0}/bash/rm.sh".format(settings.BASE_DIR), "{0}/dim/{1}".format(fact.storage.name, fact.name)]):
                print("RM: ", line)
            for line in run_command(["{0}/bash/touch.sh".format(settings.BASE_DIR), "/{0}/dim/".format(fact.storage.name), fact.name]):
                print("TOUCH: ", line)
            file = request.FILES['file']
            #for line in file.readlines():
            #   print(line)
            values = open('dim{0}'.format(fact.id), 'w')
            for line in file.readlines():
                values.write(line.decode())
            values.close()
            for line in run_command(["{0}/bash/append_file.sh".format(settings.BASE_DIR), 'dim{0}'.format(fact.id), "/{0}/dim/{1}".format(fact.storage.name, fact.name)]):
                print("APPEND: ", line)
            return HttpResponseRedirect('/detail/{0}/'.format(fact.storage.id))
    else:
        form = UploadForm()
    return render(request, 'storage/upload_values.html', {'form': form, 'fact': fact})


def delete(request, id):
    try:
        storage = models.Storage.objects.get(id=id)
        storage.delete()
        # Удаляем хранилище, если оно существовало
        for line in run_command(["{0}/bash/rm.sh".format(settings.BASE_DIR), storage.name]):
            print("RM: ", line)
    except models.Storage.DoesNotExist:
        return HttpResponse("Storage does not exist")
    return HttpResponseRedirect('/list/')


def history(request, id):
    storage = models.Storage.objects.get(id=id)
    history_list = models.History.objects.filter(storage=id)
    return render(request, 'storage/history.html', {'storage': storage, 'history_list': history_list})


def delete_history(request, id):
    h = models.History.objects.get(id=id)
    h.delete()
    return HttpResponseRedirect('/history/{0}'.format(h.storage.id))


def benchmark(request):
    return render(request, 'storage/benchmark.html')


def mysql_bench(request):
    context = mysql.connector.connect(host='46.101.221.110', user='admin', password='admin', database='vos')
    cursor = context.cursor()

    add_vos = ('select sql_no_cache v.id, c.name, p.name, m.name, vos from vos v join city c on v.city = c.id join product p on v.product = p.id join month m on v.month = m.id')

    start_time = timeit.default_timer()
    cursor.execute(add_vos)

    file = open('result', 'w')
    for (id, city, product, month, vos) in cursor:
        file.write('{0} {1} {2} {3} {4}\n'.format(id, city.strip(), product.strip(), month.strip(), vos))
    elapsed = timeit.default_timer() - start_time

    cursor.close()
    context.close()

    return render(request, 'storage/benchmark_result.html', {'title': 'Тестирование хранилища на основе MySQL','elapsed': '{0:.3f}'.format(elapsed)})


def hadoop_bench(request):

    start_time = timeit.default_timer()

    for line in run_command(["hadoop", "jar", "{0}/jar/MFRJ.jar".format(settings.BASE_DIR), "MFRJ", "/benchmark", "4", "[]"]):
        print("RUN: ", line)

    for line in run_command(["hadoop", "fs", "-get", "/{0}/output/part*".format("benchmark")]):
         print("GET: ", line)

    # filename = "{0} {1}".format("benchmark", datetime.now())
    # res = open("{0}/{1}".format(settings.BASE_DIR, filename), "a+")
    # for file in listdir(settings.BASE_DIR):
    #     if file.startswith("part"):
    #         print(file)
    #         part = open(file, 'r')
    #         for line in part.readlines():
    #             num, json = line.split('\t')
    #             res.write(json)
    #         part.close()
    #         remove(file)
    # history = models.History.objects.create(storage=storage, user=request.user)
    # history.result.save(filename, File(res))
    # history.save()
    # res.close()
    # remove("{0}/{1}".format(settings.BASE_DIR, filename))

    elapsed = timeit.default_timer() - start_time

    for line in run_command(["{0}/bash/rm.sh".format(settings.BASE_DIR), "{0}/output/".format("benchmark")]):
        print("RM: ", line)

    return render(request, 'storage/benchmark_result.html', {'title': 'Тестирование хранилища на основе Hadoop', 'elapsed': '{0:.3f}'.format(elapsed)})