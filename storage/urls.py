"""
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from storage import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^$', login_required(views.index, login_url='/login/'), name='index'),
    url(r'^list/$', login_required(views.list, login_url='/login/'), name='list'),
    #url(r'^api/list/$', views.list, name='list'),
    url(r'^result/$', login_required(views.result, login_url='/login/'), name='result'),
    url(r'^query/(?P<id>[0-9]+)/$', login_required(views.query, login_url='/login/'), name='query'),
    url(r'^detail/(?P<id>[0-9]+)/$', login_required(views.detail, login_url='/login/'), name='detail'),
    url(r'^create/$', login_required(views.create, login_url='/login/'), name='create'),
    url(r'^save/$', login_required(views.save, login_url='/login/'), name='save'),
    url(r'^initialize/(?P<id>[0-9]+)/$', login_required(views.initialize, login_url='/login/'), name='initialize'),
    #url(r'^run/(?P<id>[0-9]+)/$', login_required(views.run, login_url='/login/'), name='run'),
    #url(r'^api/login/$', csrf_exempt(views.login), name='login'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', login_required(views.logout, login_url='/login/'), name='logout'),
    url(r'^upload/(?P<id>[0-9]+)/$', login_required(views.upload_data, login_url='/login/'), name='upload'),
    url(r'^delete/(?P<id>[0-9]+)/$', login_required(views.delete, login_url='/login/'), name='delete'),
    url(r'^upload_values/(?P<id>[0-9]+)/$', login_required(views.upload_values, login_url='/login/'), name='upload_values'),
    url(r'^history/(?P<id>[0-9]+)/$', login_required(views.history, login_url='/login/'), name='history'),
    url(r'^delete_history/(?P<id>[0-9]+)/$', login_required(views.delete_history, login_url='/login/'), name='delete_history'),
    url(r'^benchmark/$', login_required(views.benchmark, login_url='/login/'), name='benchmark'),
    url(r'^mysql_bench/$', login_required(views.mysql_bench, login_url='/login/'), name='mysql_bench'),
    url(r'^hadoop_bench/$', login_required(views.hadoop_bench, login_url='/login/'), name='hadoop_bench'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
