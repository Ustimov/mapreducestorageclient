from django.db import models
from django.contrib.auth.models import User


class Storage(models.Model):
    name = models.CharField(max_length=255)
    datetime = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)

    def __str__(self):
        return self.name


class Fact(models.Model):
    name = models.CharField(max_length=255)
    storage = models.ForeignKey(Storage)
    is_fk = models.BooleanField(default=False)
    # type?

    def __str__(self):
        return self.name


class Dimension(models.Model):
    value = models.CharField(max_length=255)
    fact = models.ForeignKey(Fact)

    def __str__(self):
        return self.value


class History(models.Model):
    user = models.ForeignKey(User)
    datetime = models.DateTimeField(auto_now_add=True)
    storage = models.ForeignKey(Storage)
    result = models.FileField(upload_to='results/')