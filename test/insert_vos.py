from __future__ import print_function
import sys
import mysql.connector

context = mysql.connector.connect(user='root', password=sys.argv[1], database='vos')
cursor = context.cursor()

add_vos = ('insert into vos(month, product, city, vos) values(%s, %s, %s, %s)')

file = open('1M', 'r')
for line in file.readlines():
	values = tuple(line.split(' '))
	cursor.execute(add_vos, values)

file.close()
context.commit()
cursor.close()
context.close()
