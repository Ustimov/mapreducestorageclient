from random import randint

file = open('1M', 'w')
for i in range(1 * 10 ** 6):
    file.write('{0} {1} {2} {3}\n'.format(randint(1, 12), randint(1, 1000), randint(1, 100), randint(100, 10000)))
    if i % 100000 == 0:
        print(i)
