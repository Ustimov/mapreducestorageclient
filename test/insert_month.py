from __future__ import print_function
import sys
import mysql.connector

context = mysql.connector.connect(user='root', password=sys.argv[1], database='vos')
cursor = context.cursor()

add_month = ('insert into month(name) values(%s)')

file = open('month.txt', 'r')
for line in file.readlines():
	cursor.execute(add_month, (line,))

file.close()
context.commit()
cursor.close()
context.close()
