import sys
import json

file = open(sys.argv[1], 'r')
fk = open('fk', 'w')
manager = open('manager', 'w')
for line in file.readlines():
    values = line.split(' ')
    fk.write(json.dumps({
        'month': values[0],
        'manager': values[1],
        'city': values[2]}) + '\n')
    manager.write(values[3])

file.close()
fk.close()
manager.close()
