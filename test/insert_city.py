from __future__ import print_function
import sys
import mysql.connector

context = mysql.connector.connect(user='root', password=sys.argv[1], database='vos')
cursor = context.cursor()

add_city = ('insert into city(name) values(%s)')

file = open('city.txt', 'r')
for line in file.readlines():
	cursor.execute(add_city, (line,))

file.close()
context.commit()
cursor.close()
context.close()
