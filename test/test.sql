use vos;

create table month(
	id integer primary key auto_increment,
	name varchar(255) not null unique
);

create table product(
	id integer primary key auto_increment,
	name varchar(255) not null unique
);

create table city(
	id integer primary key auto_increment,
	name varchar(255) not null unique
);

create table vos(
	id integer primary key auto_increment,
	month integer,
	product integer,
	city integer,
	vos integer not null,
	foreign key (month) references month(id),
	foreign key (product) references product(id),
	foreign key (city) references city(id)
);