#!/bin/sh
# $1 - Project name, $2 - Attr count, $3 - Selectors

get_script_dir () {
     SOURCE="${BASH_SOURCE[0]}"
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     $( cd -P "$( dirname "$SOURCE" )" )
     pwd
}
#cd foobar
echo "$(get_script_dir)"

#hadoop jar ../jar/MFRJ.jar MFRJ $1 $2 $3
