module.exports = function (grunt) {

    grunt.initConfig({

        shell: {
            dev: {
                command: 'python manage.py runserver'
            }
        },

        watch: {
            files: 'templates/*.xml',
            tasks: ['fest']
        },

        concurrent: {
            target: {
                tasks: ['shell:dev', 'watch'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },

        fest: {
            templates: {
                files: [{
                    expand: true,
                    cwd: 'templates',
                    src: '*.xml',
                    dest: 'storage/static/storage/js/tmpl'
                }],
                options: {
                    template: function (data) {
                        return grunt.template.process(
                            'define(function () { return <%= contents %> ; });',
                            {data: data}
                        );
                    }
                }
            }
        },

        qunit: {
            all: ['public_html/tests/*.html']
        }

    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-fest');
    grunt.loadNpmTasks('grunt-contrib-qunit');

    grunt.registerTask('default', ['concurrent:target']);
    grunt.registerTask('test', ['qunit']);

};
